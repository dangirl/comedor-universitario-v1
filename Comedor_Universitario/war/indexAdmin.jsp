<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="clases.*"%>
<%@ page import="java.util.List"%>

<%
	List<Pensionista> pensionistas = (List<Pensionista>) request.getAttribute("pensionistas");
%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="http://code.jquery.com/jquery-latest.js"></script>


<title>Comedor Universitario</title>

<!-- css -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet"
	type="text/css" />
<link href="css/nivo-lightbox.css" rel="stylesheet" />
<link href="css/nivo-lightbox-theme/default/default.css"
	rel="stylesheet" type="text/css" />
<link href="css/owl.carousel.css" rel="stylesheet" media="screen" />
<link href="css/owl.theme.css" rel="stylesheet" media="screen" />
<link href="css/flexslider.css" rel="stylesheet" />
<link href="css/animate.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet" />
<link href="color/default.css" rel="stylesheet">


</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">



	<!-- Navigation -->
	<div id="navigation">
		<nav class="navbar navbar-custom" role="navigation">
			<div class="container">
				<div class="row">
					<div class="col-md-2">
						<div class="site-logo">
							<a href="index.html" class="brand">Bienvenido Administrador</a>
						</div>
					</div>


					<div class="col-md-10">

						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle"
								data-toggle="collapse" data-target="#menu">
								<i class="fa fa-bars"></i>
							</button>
						</div>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="menu">
							<ul class="nav navbar-nav navbar-right">

								<li class="active"><a href="#estadistica">Voto</a></li>
								<li class="active"><a href="cerrarSesion">Salir</a></li>


								<li><a href="#addUser">Agregar Usuario</a></li>
								<li><a href="#lis_pensionista">Pensionista</a></li>
							</ul>
						</div>
						<!-- /.Navbar-collapse -->

					</div>
				</div>
			</div>
			<!-- /.container -->
		</nav>
	</div>
	<!-- /Navigation -->

	<!-- Section: about -->
	<section id="estadistica" class="home-section color-dark bg-white">
		<div class="container marginbot-50">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="wow flipInY" data-wow-offset="0" data-wow-delay="0.4s">
						<div class="section-heading text-center">
							<h2 class="h-bold">Votos por Dia</h2>
							<div class="divider-header"></div>
							<p>Las comidas mas votadas hoy 29 de mayo 2016.</p>
						</div>
					</div>
				</div>
			</div>

		</div>

		<div class="container">


			<div class="row">

				<div class="col-md-6">
					<img src="img/dummy1.jpg" alt="" class="img-responsive" />

				</div>

				<div class="col-md-6">
					<p>Porcentaje de las comidas de hoy.</p>
					<div class="progress progress-striped active">
						<div class="progress-bar progress-bar-success" role="progressbar"
							aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
							style="width: 40%">40% - Lentejitas</div>
					</div>
					<div class="progress progress-striped active">
						<div class="progress-bar progress-bar-info" role="progressbar"
							aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
							style="width: 20%">20% - Olluquito</div>
					</div>
					<div class="progress progress-striped active">
						<div class="progress-bar progress-bar-warning" role="progressbar"
							aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
							style="width: 60%">60% - Milanesa de Pollo</div>
					</div>
					<div class="progress progress-striped active">
						<div class="progress-bar progress-bar-danger" role="progressbar"
							aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"
							style="width: 80%">80% - Pollo Broaster</div>
					</div>

				</div>


			</div>
		</div>

	</section>
	<!-- /Section: about -->


	<!-- Section: contact -->
	<section id="addUser"
		class="home-section nopadd-bot color-dark bg-gray text-center">
		<div class="container marginbot-50">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="wow flipInY" data-wow-offset="0" data-wow-delay="0.4s">
						<div class="section-heading text-center">
							<h2 class="h-bold">Ingrese Nuevo Usuario</h2>

						</div>
					</div>
				</div>
			</div>

		</div>

		<div class="container">
			<!-- registrar usuario -->
			<div class="row marginbot-80">
				<div class="col-md-8 col-md-offset-2">
					<form class="wow bounceInUp" data-wow-offset="10"
						data-wow-delay="0.2s">
						<div class="row marginbot-20">
							<div class="col-md-6 xs-marginbot-20">
								<input type="number" class="form-control input-lg" name="cui"
									id="cui" placeholder="CUI" required="required" value="20101256" />
							</div>
							<div class="col-md-6">
								<input type="number" class="form-control input-lg" name="dni"
									id="dni" placeholder="DNI" required="required" value="705687" />
							</div>
						</div>

						<div class="row xs-marginbot-20">
							<div class="col-md-6 xs-marginbot-20">
								<input type="text" class="form-control input-lg" name="apaterno"
									id="apaterno" placeholder="Apellido Paterno"
									required="required" />
							</div>
							<div class="col-md-6">
								<input type=text class="form-control input-lg" name="amaterno"
									id="amaterno" placeholder="Apellido Materno"
									required="required" />
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<input type="text" class="form-control input-lg" name="name"
										id="name" placeholder="Nombre" required="required" />
								</div>
								<select name="estado" id="estado">
									<option value="1">Activo</option>
									<option value="2" Select>Inactivo</option>
									<option value="3">Pendiente</option>
								</select><br>
								<button type="submit" class="btn btn-skin btn-lg btn-block"
									id="registrar">Registrar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	<!--  List Pensionista -->

	<section id="lis_pensionista"
		class="home-section nopadd-bot color-dark bg-gray text-center">
		<div class="container">
			<div class="row marginbot-80">

				<h4>Lista de Pensionistas</h4>

				<div class="col-md-2 col-md-offset-2"></div>
				<div class="col-md-8 col-md-offset-2" id="table_Usuario">

					<table class="table table-hover">
						<thead>
							<tr>
								<th>Nro</th>
								<th>CUI</th>
								<th>Nombre</th>
								<th>Apellidos</th>
								<th>Escuela</th>
								<th>DNI</th>
								<th>E-mail</th>
								<th>Estado</th>
								<th>Select</th>
								<th>Update</th>
							</tr>
						</thead>
						<tbody>
							<%
								if (pensionistas != null) {

									int i = 1;
									for (Pensionista p : pensionistas) {
							%>
							<tr>
								<td><%=i%></td>
								<td><%=p.getCui()%></td>
								<td><%=p.getName()%></td>
								<td><%=p.getApellidos()%></td>
								<td><%=p.getEscuela()%></td>
								<td><%=p.getDni()%></td>
								<td><%=p.getEmail()%></td>
								<td><%=p.getNameEstado()%></td>
								<td><input type="checkbox" name="option[]" id="option[] "
									value='<%=p.getIdPensionista()%>' /></td>
								<td><button type="button" name="_actualizar"
										class="_update" value='<%=p.getIdPensionista()%>' />Update
									</button></td>

							</tr>
							<%
								i++;
									}
								}
							%>

						</tbody>
					</table>

				</div>
				<div class="col-md-2 col-md-offset-2"></div>

			</div>
			
			<div class="row marginbot-80">
				<div class="col-md-2 col-md-offset-2"></div>
				<div class="col-md-8 col-md-offset-2">						
					<button type="button" data-toggle="modal"
						data-target="#modal_eliminar">Eliminar</button>
				</div>
				<div class="col-md-2 col-md-offset-2"></div>
			</div>
			<div class="fila"></div>
		</div>

	</section>
	<!-- /Section: contact -->

<section>
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">

					<div class="text-center">
						<a href="#estadistica" class="totop"><i
							class="fa fa-angle-up fa-3x"></i></a>

						<p>
							Peru, Avenida Alfonso Ugarte, 121 Jesus Maria Paucarpata,
							Arequipa<br /> &copy;Copyright 2016 - Shuffle. Designed by <a
								href="http://facebook.com/mauricioMaldonado">Mauricio
								Maldonado</a>
						</p>

					</div>
				</div>
			</div>
		</div>
	</footer>
</section>
	<!-- Core JavaScript Files -->



	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.sticky.js"></script>
	<script src="js/jquery.flexslider-min.js"></script>
	<script src="js/jquery.easing.min.js"></script>
	<script src="js/jquery.scrollTo.js"></script>
	<script src="js/jquery.appear.js"></script>
	<script src="js/stellar.js"></script>
	<script src="js/wow.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/nivo-lightbox.min.js"></script>

	<script src="js/custom.js"></script>
	<script>
		$(document).ready(function() {
			//registrar usuario
			$('#registrar').click(function(event) {
				var varCui = $('#cui').val();
				var varDni = $('#dni').val();
				var varPat = $('#apaterno').val();
				var varName = $('#name').val();
				var varMat = $('#amaterno').val();
				var varEstado = $('#estado').val();
				alert("registrando!");
				$.get('registrarUsuario', {
					cui : varCui,
					dni : varDni,
					apaterno : varPat,
					amaterno : varMat,
					name : varName,
					estado : varEstado

				}, function(responseText) {
					$('#table_usuario').html(responseText);
				});

			});

			//actualizar
			$('._update').click(function(event) {
				var val = $(this).val();

				$.get('recoveservicio', {
				//clave:val
				}, function(responseText) {
					$('#update_servicio').html(responseText);
				});

			});

			//eliminar
			$('#_eliminar').click(function(event) {

				var varIds = "";

				$("input[name='option[]']:checked").each(function() {
					varIds += ($(this).val()) + " ";
				});
				$.get('deletePensionista', {
					valores : varIds
				}, function(responseText) {

					$('#table_Usuario').html(responseText);
				});

			});

			//agregar
			

		});
	</script>

	<!-- Modal Delete -->

	<div class="modal fade" id="modal_eliminar" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Eliminar</h4>
				</div>
				<div class="modal-body">
					<p>Esta Seguro de Borrar?</p>
				</div>
				<div class="modal-footer">
					<button type="button" data-dismiss="modal">Cancelar</button>
					<button type="button" data-dismiss="modal" id="_eliminar">Eliminar</button>
				</div>
			</div>
		</div>
	</div>


	<!-- fin delete -->


</body>

</html>