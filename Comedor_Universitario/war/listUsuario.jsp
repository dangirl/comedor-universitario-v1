<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="clases.*"%>
<%@ page import="java.util.List"%>

<%
	List<Usuario> usuarios = (List<Usuario>) request.getAttribute("usuarios");
%>

<table class="table table-hover">
	<thead>
		<tr>
			<th>Nro</th>
			<th>CUI</th>
			<th>Nombre</th>
			<th>Apellidos</th>
			<th>DNI</th>
		</tr>
	</thead>
	<tbody>
		<%
		if(usuarios.size()!=0){
			
			int i = 1;
			for (Usuario us : usuarios) {
		%>
		<tr>
			<td><%=i%></td>
			<td><%=us.getCui()%></td>
			<td><%=us.getName()%></td>
			<td><%=us.getApellidos()%></td>
			<td><%=us.getDni()%></td>
		</tr>
		<%
			i++;
			}
		}
		%>
	</tbody>
</table>




