package servlets;
import clases.*;
import javax.jdo.Query;
import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class RegistrarPensionista extends HttpServlet {

	protected void doGet(HttpServletRequest req,
			HttpServletResponse resp) throws ServletException, IOException {

		resp.setContentType("text/html");
		String cui = req.getParameter("cui2");
		String escuela = req.getParameter("escuela");
		String  email= req.getParameter("email");
		String telefono = req.getParameter("telefono");
		String pass = req.getParameter("pass");		

		PersistenceManager pm = PMF.get().getPersistenceManager();

		final Query q = pm.newQuery(Usuario.class);
		Pensionista pensionista = new Pensionista( escuela,email,telefono, pass, 1);

		List<Usuario> usuarios = (List<Usuario>) q.execute();
		String i="no encontrado";

		for(Usuario us : usuarios){


			String _cui = us.getCui();
			
			if(cui.equals(_cui)){
				pensionista.setUser(us);
				i="encontrado";
				break;
			}
		}
		if (i.equals("encontrado")) {
			try{
				pm.makePersistent(pensionista);
				resp.getWriter().println("Datos grabados correctamente ");
				System.out.println(pensionista.getPassword());

			}catch(Exception e){
				System.out.println(e);
				resp.getWriter().println("Ocurrio un error, <a href='index.html'>vuelva a intentarlo</a>");
			}finally{
				pm.close();
			}

		}
		else{
			resp.getWriter().println("Datos no grabados!");
		}
	}
}
