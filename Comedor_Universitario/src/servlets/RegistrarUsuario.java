package servlets;

import clases.*;

import java.io.IOException;
//import java.io.PrintWriter;

import javax.jdo.PersistenceManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@SuppressWarnings("serial")
public class RegistrarUsuario extends HttpServlet {

	protected void doGet(HttpServletRequest req,
			HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html");
		
		//PrintWriter out = resp.getWriter();
		
			String cui = req.getParameter("cui");
			String name = req.getParameter("name");
			String apaterno = req.getParameter("apaterno");
			String amaterno = req.getParameter("amaterno");
			String dni = req.getParameter("dni");			
			String estado = req.getParameter("estado");
			
			Usuario user = new Usuario(cui,name,apaterno, amaterno,dni,Integer.parseInt(estado));
			
			PersistenceManager pm = PMF.get().getPersistenceManager();
			try{
				pm.makePersistent(user);
				resp.sendRedirect("/listUsuario.jsp");
		
			}catch(Exception e){
				System.out.println(e);
				resp.getWriter().println("Ocurrio un error, <a href='index.html'>vuelva a intentarlo</a>");
			}finally{
				pm.close();
			}
		
		
	}
}
