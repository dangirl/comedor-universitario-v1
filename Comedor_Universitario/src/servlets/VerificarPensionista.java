package servlets;
import clases.*;
import javax.jdo.Query;
import java.io.IOException;
//import java.io.PrintWriter;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class VerificarPensionista extends HttpServlet {

	protected void doGet(HttpServletRequest req,
			HttpServletResponse resp) throws ServletException, IOException {
		
		resp.setContentType("text/html");
		String cui = req.getParameter("cui");		

		PersistenceManager pm = PMF.get().getPersistenceManager();
		
		final Query q = pm.newQuery(Usuario.class);
			
		List<Usuario> usuarios = (List<Usuario>) q.execute();
		String i="no encontrado";
		
		for(Usuario us : usuarios){

			
			String _cui = us.getCui();

			
			if(cui.equals(_cui)){
				i="encontrado";
				break;
			}
		}
		if (i.equals("encontrado")) {
			resp.sendRedirect("respuesta.jsp");
		}
		else{
			resp.getWriter().println("<p>No Encontrado con JavaScript</p>");
		}
	}
		
}
