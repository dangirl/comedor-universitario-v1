package servlets;
import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import clases.PMF;
import clases.Pensionista;


	@SuppressWarnings("serial")
	public class PensionistaServlet extends HttpServlet{
		@Override
		protected void doGet(HttpServletRequest req, HttpServletResponse resp)
				throws ServletException, IOException {
			resp.setContentType("text/html");
			
			final PersistenceManager pm = PMF.get().getPersistenceManager();
			final Query q = pm.newQuery(Pensionista.class);
			q.setOrdering("cui ascending");
			//q.setRange(0, 10);
			try{
				@SuppressWarnings("unchecked")
				List<Pensionista> pensionistas = (List<Pensionista>) q.execute();
				req.setAttribute("pensionistas", pensionistas);
				RequestDispatcher rd = req.getRequestDispatcher("/indexAdmin.jsp");
				rd.forward(req, resp);
			}catch(Exception e){
				System.out.println(e);
			}finally{
				q.closeAll();
				pm.close();
			}
		}			
	}