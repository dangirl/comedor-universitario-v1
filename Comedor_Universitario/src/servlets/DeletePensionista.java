package servlets;

import java.awt.List;
import java.io.IOException;
import java.util.ArrayList;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.KeyFactory;

import clases.PMF;
import clases.Pensionista;



@SuppressWarnings("serial")
public class DeletePensionista extends HttpServlet{
	@Override

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		String value= req.getParameter("valores");
		
		resp.setContentType("text/html");
		if(value!=null){
			ArrayList<String> vals = new ArrayList<String>();
			int i=0,j=0;
			while(i<value.length()){
				if(value.charAt(i)==32){
					vals.add(value.substring(j, i));
					j=i+1;				
				}	
				i++;
			}
			final PersistenceManager pm = PMF.get().getPersistenceManager();			
			final Query p3 = pm.newQuery(Pensionista.class);

			Transaction tx = pm.currentTransaction();
			tx.begin();		
			

			try{
				try{						
					//elimina todos los pensionistas que selecciono
					for(int k=0;k<vals.size();k++){						
						Pensionista found = pm.getObjectById(Pensionista.class,KeyFactory.stringToKey(vals.get(k)));
						pm.deletePersistent(found);	
					}

					tx.commit();					
					resp.sendRedirect("pensionistaServlet");
				}catch(Exception e){
					System.out.println(e);
					resp.getWriter().println("Ocurrió un error, vuelva a intentarlo.");
					resp.sendRedirect("pensionistaServlet");
				}

			}catch(Exception e){
				System.out.println(e);
			}finally{
				try {
					if (tx.isActive())
						tx.rollback();
				} finally {
					pm.close();
				}}

		}
	}
}



