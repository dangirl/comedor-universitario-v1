package clases;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.datanucleus.annotations.Unowned;

@PersistenceCapable
public class Pensionista {
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key idPensionista;	
	 
	@Persistent
	private String escuela;
	@Persistent 
	private String email;
	@Persistent 
	private String telefono;
	@Persistent
	private String password;
	@Persistent
	private int estado;
	@Persistent
	private String cui;
	
	
	@Persistent
	@Unowned
	protected Usuario user;

	public Pensionista(String escuela, String email, String telefono, String password, int estado) {
		super();
		this.escuela = escuela;
		this.email = email;
		this.telefono = telefono;
		this.password = password;
		this.estado = estado;
		this.user =null;
		this.cui = null;
	}
	
	

	public String getIdPensionista() {
		return KeyFactory.keyToString(idPensionista);

	}

	

	public String getEscuela() {
		return escuela;
	}
	
	public String getApellidos() {
		return user.getPat() + " "+user.getMat();
	}
	
	public String getNameEstado(){
		//String nombreEstado="";
		switch(this.estado){
		case 1: return "Activo";
		case 2: return "Inactivo";
		case 3: return "Pendiente";
		default: return "";
		}
		
	}
	
	
	

	public String getName() {
		return user.getName();
	}

	public void setEscuela(String escuela) {
		this.escuela = escuela;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public Usuario getUser() {
		return user;
	}

	public void setUser(Usuario user) {
		this.user = new Usuario(user);
		this.cui=user.getCui();
	}	
	
	public String getApellidoP(){
		return user.getPat();
	}
	
	public String getApellidoM(){
		return user.getMat();
	}
	

	public String getDni(){
		return user.getDni();
	}
	
	public String getCui(){
		return user.getCui();
	}

	
	
	
}
